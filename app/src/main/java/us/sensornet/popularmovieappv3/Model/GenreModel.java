package us.sensornet.popularmovieappv3.Model;

@SuppressWarnings("ALL")
public class GenreModel {

    public Integer mId;
    public String mName;

    public GenreModel() {
    }

    public GenreModel(int id, String name) {
        mId = id;
        mName = name;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

}