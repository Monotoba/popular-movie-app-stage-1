package us.sensornet.popularmovieappv3.Api;

import android.graphics.Movie;

import java.util.List;

import us.sensornet.popularmovieappv3.Model.MovieListModel;
import us.sensornet.popularmovieappv3.Model.MovieModel;

public interface ServiceInterface {
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListReady(MovieListModel movieList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataReady(List<MovieModel> movies);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieDetailsReady(Movie movie);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieDetailsFailure(Throwable t);
}
