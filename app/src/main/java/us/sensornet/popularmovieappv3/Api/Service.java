package us.sensornet.popularmovieappv3.Api;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.sensornet.popularmovieappv3.BuildConfig;
import us.sensornet.popularmovieappv3.Model.MovieListModel;
import us.sensornet.popularmovieappv3.Model.MovieModel;


@SuppressWarnings("SpellCheckingInspection")
public class Service {
    private static OkHttpClient mClient;
    //private static Request mRequest;
    private static GsonConverterFactory mGsonFactory;
    private final String TAG = getClass().getSimpleName();
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private final Context mContext;
    private final String API_BASE_URL = "https://api.themoviedb.org";
    private final String API_KEY = BuildConfig.theMovieDB3_Api_Key;
    private final List<ServiceInterface> mListeners;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private final Boolean mIsLogging;

    public Service(Context context, Boolean logging) {
        mContext = context;
        mIsLogging = logging;
        mListeners = new ArrayList<>();

        if (null == mClient) {
            mClient = new OkHttpClient();
        }


        // Create a single instance of GsonConverterFactory
        if (null == mGsonFactory) {
            mGsonFactory = GsonConverterFactory.create();
        }
    }

    public void popularMovieRequest(String mode) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);

        Call<MovieListModel> call = moviesClient.getMovies(mode, API_KEY);

        // Use call object to make request
        call.enqueue(new Callback<MovieListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieListModel> call, Response<MovieListModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, "Retrofit call made successfully!");

                    MovieListModel movies = response.body();

                    // Pass back only the MovieList
                    try {
                        //noinspection ConstantConditions
                        notifyMovieListDataReady(movies.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unseucessful....");
                notifyMovieListFailure(t);
            }
        });
    }

    public void addListener(ServiceInterface listener) {
        mListeners.add(listener);
    }

    @SuppressWarnings("unused")
    public void movieDetailsRequest(String movieId) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).client(mClient).addConverterFactory(mGsonFactory).build();

        MovieClient moviesClient = retrofit.create(MovieClient.class);

        Call<MovieListModel> call = moviesClient.getMovies("", API_KEY);

        // Use call object to make request
        call.enqueue(new Callback<MovieListModel>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<MovieListModel> call, Response<MovieListModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, "Retrofit call made successfully!");

                    MovieListModel movies = response.body();

                    // Pass back only the MovieList
                    try {
                        //noinspection ConstantConditions
                        notifyMovieListDataReady(movies.getResults());
                    } catch (NullPointerException e) {
                        Log.d(TAG, "Network query results cannot be parse into MovieListModel Object");
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                } else {
                    Log.d(TAG, "Retrofit Error: " + response.message() + " " + response.errorBody());
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<MovieListModel> call, Throwable t) {
                Log.d(TAG, "Error: Retrofit call unsucessful....");
                notifyMovieListFailure(t);
            }
        });

    }

    public static class QueryModes {
        public static final String MostPopular = "popular";
        public static final String TopRated = "top_rated";
    }


    private void notifyMovieListFailure(Throwable t) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieListFailure(t);
        }
    }


    private void notifyMovieListDataReady(List<MovieModel> movies) {
        for (ServiceInterface listener : mListeners) {
            listener.OnMovieListDataReady(movies);
        }
    }

}
