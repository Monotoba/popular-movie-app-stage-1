package us.sensornet.popularmovieappv3;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import us.sensornet.popularmovieappv3.Helper.GenreHelper;
import us.sensornet.popularmovieappv3.Helper.ImageHelper;
import us.sensornet.popularmovieappv3.Model.MovieModel;

public class DetailActivity extends AppCompatActivity {
    @SuppressWarnings("unused")
    private final String TAG = getClass().getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private MovieModel mMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Gson gson = new Gson();
        String strJson = getIntent().getStringExtra("jsonMovie");
        mMovie = gson.fromJson(strJson, MovieModel.class);
        Objects.requireNonNull(mMovie, "mMovie not initialized in onCreate");
        showMovieDetails(mMovie);
    }


    private void showMovieDetails(@NonNull MovieModel movie) {
        ImageView imageView = findViewById(R.id.img_poster);
        TextView tvTitle = findViewById(R.id.tv_title);
        TextView tvReleaseDate = findViewById(R.id.tv_release_date);
        TextView tvAverageVotes = findViewById(R.id.tv_average_vote);
        TextView tvSynopsis = findViewById(R.id.tv_synopsis);
        TextView tvGenres = findViewById(R.id.tv_genre);

        String Url = ImageHelper.ImageWidth.w500 + "/" + movie.getPosterPath();
        imageView.setTag(movie.getId());
        ImageHelper.getDrawableFromNetwork(imageView, Url);

        GenreHelper genreHelper = new GenreHelper();

        // Genres
        StringBuilder strGenres = new StringBuilder();
        List<Integer> genres = movie.getGenreIds();
        for (int id : genres) {
            strGenres.append(genreHelper.getGenreById(id).getName()).append(", ");
        }

        // Trim trailing ", " if any genres have been added
        if (strGenres.length() > 3) {
            strGenres = new StringBuilder(strGenres.substring(0, strGenres.length() - 2));
        }

        // Load the view with values
        tvTitle.setText(movie.getTitle());
        tvReleaseDate.setText(movie.getReleaseDate());
        tvAverageVotes.setText(String.format(Locale.getDefault(), "%.1f", movie.getVoteAverage()));
        tvSynopsis.setText(movie.getOverview());

        // If we have no genres there is
        // no point in displaying the header...
        if (strGenres.length() == 0) {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.INVISIBLE);
            tvGenres.setVisibility(View.INVISIBLE);
        } else {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.VISIBLE);
            tvGenres.setVisibility(View.VISIBLE);
            tvGenres.setText(strGenres.toString());
        }

    }
}
